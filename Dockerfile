FROM ruby:2.3.4
RUN apt-get update -qq
RUN apt-get install -y locales build-essential libpq-dev nodejs git imagemagick
RUN dpkg-reconfigure locales && \
  locale-gen C.UTF-8 && \
  /usr/sbin/update-locale LANG=C.UTF-8
RUN echo 'en_US.UTF-8 UTF-8' >> /etc/locale.gen && locale-gen
ENV LC_ALL C.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8
RUN mkdir /ninelifes
WORKDIR /ninelifes
RUN git clone https://github.com/vishnubob/wait-for-it.git
ADD Gemfile /ninelifes/Gemfile
ADD Gemfile.lock /ninelifes/Gemfile.lock
RUN bundle config github.https true
RUN bundle install
ADD . /ninelifes

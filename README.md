A portal for a creative people to share and discuss their achievements.

* PostgreSQL backed Rails with few Javascript hand-written components
* My CSS3-styling from scratch with Responsive design in every word
* Google|Facebook|EmailActivation Login system
* Visual oriented catalog
* Item owning (creation)
* Item description with scalable photos browser
* Comments, Tags and Bookmarking
* Administrative accounts
* Messenger for linked accounts communication with groups support and PUSH notifications

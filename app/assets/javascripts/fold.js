function fold(root){
  if (typeof root === "object") {
    var append = (root.append || root.appendChild).bind(root);
    root.pics=new Rotator((function(){
        var is=root.getElementsByTagName("img");
        var i=is.length;
        var a=new Array(i);
        while (i--){
          a[i] = root.removeChild(is[i]).src;
        }
        return a;
      })());
  }
  else return false;
  var Fold = (function Fold(imagessource){	    //new constructor
		this.top=new element("div.img.top");
		this.bottom=new element("div.img.bottom");
		this.imagessource=imagessource;
    append(this.top);
    append(this.bottom);
		this.Load = function(){
	    if (!this.top.classList.contains("face")) {
				this.loaded=false;
				nl_ajax({
				  method: "GET",
				  url: this.imagessource.current(),
				  type: "blob",
				  resulthandler: function(xhr){
						if (xhr.status == 200) {
							if (this.picdata) URL.revokeObjectURL(this.picdata);
							this.picdata=URL.createObjectURL(xhr.response);
							this.setPic("url('"+this.picdata+"#"+new Date().getTime()+"')");
							this.loaded=true;
							this.toFace();
						}
					}.bind(this),
					progresshandler: document.getElementById("logo_progress"),
				});
			}
		};
		//this.top.addEventListener("animationend",this.Load.bind(this));
		this.bottom.addEventListener("click",function(e){
			this.imagessource.next();
			this.fold();
		}.bind(this));
		this.fold = function(){
		  this.other.Load();
		}
	}).extends({	    //base object
	    //methods
		setPic(pic){
			this.top.style.backgroundImage = pic;
			this.bottom.style.backgroundImage = pic;
			return this;
		},  
		toFace(){
			this.top.classList.add("face");
			this.bottom.classList.add("face");
			if (this.other) {
				this.other.top.classList.remove("face");
				this.other.bottom.classList.remove("face");
			}
			return this;
		},
		get loaded(){
			if (this.top.getAttribute("loaded") == "true") return true;
			return false;
		},
		set loaded(v){
			var b=v?"true":"false";
			this.top.setAttribute("loaded",b);
			this.bottom.setAttribute("loaded",b);
		}
	});
	var imgs = new Rotator(new Array(2));
  imgs[0] = new Fold(root.pics);
  imgs[1] = new Fold(root.pics);
	imgs[0].other=imgs[1];imgs[1].other=imgs[0];
	imgs.current().Load();
	root.folds=imgs;
	return root;
}
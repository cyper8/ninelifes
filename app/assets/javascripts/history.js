$(document).on('click', 'a[data-remote=true][nav]', function(e) {
  history.pushState({fetch:this.href}, '', this.href);
});

window.addEventListener('popstate', function (e) {
  if (e.state.fetch) location.href=e.state.fetch;
});

$(function(){
  $("[unread]").on("click",function(event){
    var self = event.target;
    self.removeAttribute("unread");
  });

  $("[counter]").on("click",function(event){
    var self = event.target;
    self.removeAttribute("counter");
  });

  $("[unread_counter]").on("click",function(event){
    var self = event.target;
    self.removeAttribute("unread_counter");
  });
});
function CheckSubscription(){
  window.addEventListener("load",function(){
    navigator.serviceWorker.ready.then(function(registration){
      if (self_url && vapidPublicKey){
        console.log("Asking serviceworker to check subscription for: "+self_url);
        navigator.serviceWorker.controller.postMessage({user: self_url, vapid: vapidPublicKey.buffer});
      }
    });
  })
}

if (navigator.serviceWorker){
  // Let's check if the browser supports notifications
  if (!("Notification" in window)) {
    console.error("This browser does not support desktop notification");
  }

  // Let's check whether notification permissions have already been granted
  else if (Notification.permission === "granted") {
    console.log("Permission to receive notifications has been granted");
    CheckSubscription();
  }

  // Otherwise, we need to ask the user for permission
  else if (Notification.permission !== 'denied') {
    Notification.requestPermission(function (permission) {
      // If the user accepts, let's create a notification
      if (permission === "granted") {
        console.log("Permission to receive notifications has been granted");
        CheckSubscription();
      }
    });
  }

  navigator.serviceWorker.addEventListener("message",function(event){
    if (event.data.message) {
      if ($('section#messages').length) {
        $.get(event.data.message.tag+".js",(data) => {data});
      }
      var b = $(".button#messenger-button");
      if (b.length) {
        var c = b.attr("counter");
        b.attr("counter",parseInt(c||0)+1);
      }
    }
  })
}
else {
  console.log("No serviceworker support - not subscribing for PUSH.");
}


/* global Element, HTMLElement, HTMLProgressElement */

var $E=Element,$HE=HTMLElement,$F=Function,$p="prototype";
if (!$HE[$p].hasOwnProperty("innerText")) {
	$HE[$p].__defineSetter__("innerText",function(v){return this.textContent = v});
	$HE[$p].__defineGetter__("innerText",function(){return this.textContent});
}
$F[$p].extends = function(parent){
	if (typeof parent === "function"){
		this[$p] = new parent();
		this[$p].constructor = this;
		this[$p].parent = parent[$p];
	}
	else {
		this[$p] = parent;
		this[$p].constructor = this;
		this[$p].parent = parent;
	}
	return this;
};
$E[$p].addBCListener = function(msg,act){
	return Object.defineProperty(this,msg,{
	  value: act.bind(this),
	  configurable: true,
	  enumerable: true,
	  writable: true
	});
};
$E[$p].removeBCListener = function(msg){
  delete this[msg];
  return this;
}
$E[$p].broadcast = function(){ // and some arguments
	try {
		if (typeof this[arguments[0]] === "function") {
		  this[arguments[0]].apply(this,toA(arguments).slice(1));
		}
	}
	catch (err) {throw err}
	finally {
		for (var i=0;i<this.children.length;i++) {
			this.children[i].broadcast.apply(this.children[i],arguments);
		}
		return this;
	}
};

function Debounced(func,backoff){
	var timer;
	return function(){
		var self = this;
		var evtargs = arguments;
		if (timer){
			clearTimeout(timer);
			timer = undefined;
		}
		timer = setTimeout(function(){
			clearTimeout(timer);
			timer = undefined;
			func.apply(self,evtargs);
		},backoff);
	}
}

(function(window,document) {

    var prefix = "", _addEventListener, support;

    // detect event model
    if ( window.addEventListener ) {
        _addEventListener = "addEventListener";
    } else {
        _addEventListener = "attachEvent";
        prefix = "on";
    }

    // detect available wheel event
    support = "onwheel" in document.createElement("div") ? "wheel" : // Modern browsers support "wheel"
              document.onmousewheel !== undefined ? "mousewheel" : // Webkit and IE support at least "mousewheel"
              "DOMMouseScroll"; // let's assume that remaining browsers are older Firefox

    window.addWheelListener = function( elem, callback, useCapture ) {
        _addWheelListener( elem, support, callback, useCapture );

        // handle MozMousePixelScroll in older Firefox
        if( support == "DOMMouseScroll" ) {
            _addWheelListener( elem, "MozMousePixelScroll", callback, useCapture );
        }
    };

    function _addWheelListener( elem, eventName, callback, useCapture ) {
      elem[ _addEventListener ]( prefix + eventName, support == "wheel" ? callback : function( originalEvent ) {
        !originalEvent && ( originalEvent = window.event );

        // create a normalized event object
        var event = {
          // keep a ref to the original event object
          originalEvent: originalEvent,
          target: originalEvent.target || originalEvent.srcElement,
          type: "wheel",
          deltaMode: originalEvent.type == "MozMousePixelScroll" ? 0 : 1,
          deltaX: 0,
          deltaY: 0,
          deltaZ: 0,
          preventDefault: function() {
            originalEvent.preventDefault ?
              originalEvent.preventDefault() :
              originalEvent.returnValue = false;
          }
        };

        // calculate deltaY (and deltaX) according to the event
        if ( support == "mousewheel" ) {
          event.deltaY = - 1/40 * originalEvent.wheelDelta;
          // Webkit also support wheelDeltaX
          originalEvent.wheelDeltaX && ( event.deltaX = - 1/40 * originalEvent.wheelDeltaX );
        } else {
          event.deltaY = originalEvent.detail;
        }

        // it's time to fire the callback
        return callback( event );

      }, useCapture || false );
    }

})(window,document);

/*
**
    Basic custom ajax fuction

    arg: req = {
        method: "",
        url: "",
        type: "",
        params: "",
				mime: "",
        resulthandler: function(xhr){} | Element,
        progresshandler: function(ratio){},
        headers:{
                "name":"value",
            }
    };
*/
function nl_ajax(req){
  if (typeof req != "object") {
    throw new Error("Argument must be of type: Object");
  }
  var xhr = new XMLHttpRequest();
  xhr.request = req;
  if (typeof xhr.request.resulthandler === "undefined") {
    throw new Error("ajax: no handler function!");
  }
  xhr.addEventListener("readystatechange",xhr.handler = function(e){
    if (this.readyState == 4){
      if (typeof this.request.resulthandler === 'function'){
        this.request.resulthandler(this);
      }
      else if (typeof this.request.resulthandler === 'object'){
        this.request.resulthandler.innerHTML = this.response;
      }
    }
  });
  xhr.timeout = 30000;
  xhr.addEventListener("timeout",function(){
    if ((this.readyState > 0) && (this.readyState < 4)) this.abort();
    if (this.request.initiator) this.request.initiator.skip();
    else this.handler();
  });
  if (xhr.request.progresshandler){
    if (typeof xhr.request.progresshandler === "function") {
      (xhr.upload?xhr.upload:xhr).onprogress = function(e){
        xhr.request.progresshandler.call(xhr.request,(e.lengthComputable?(e.loaded/e.total):1));
      };
    }
    else if (xhr.request.progresshandler instanceof HTMLProgressElement) {
      (xhr.upload?xhr.upload:xhr).onprogress = function(e){
        xhr.request.progresshandler.max = e.total;
        xhr.request.progresshandler.value = e.loaded;
      };
    }
  }
  if (xhr.request.type) {
    xhr.responseType = xhr.request.type;
  }
  (xhr.executesession = function(){
    xhr.open(xhr.request.method,encodeURI(xhr.request.url),true);
    if (xhr.request.mime) {
      xhr.overrideMimeType(xhr.request.mime);
    }
    if (xhr.request.headers) {
      for (h in xhr.request.headers) {
        xhr.setRequestHeader(h,xhr.request.headers[h]);
      }
    }
    xhr.send((xhr.request.method!="GET")?xhr.request.params:undefined);
  })();
  return xhr;
}

var toA = function(is){
  if (!is.length) return [];
  if (is instanceof Array) return is;
  var i=is.length;
  var a=new Array(i);
  while (i--){
    a[i] = is[i];
  }
  return a;
};

function isScrolledIntoView(el) {
  var elemTop = el.getBoundingClientRect().top;
  var elemBottom = el.getBoundingClientRect().bottom;

  var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
  return isVisible;
}

var Rotator = (function Rotator(arr){
  var pointer=0;
  this.push.apply(this,arr);
  this.rotate = function(by){
    if (by!=0){
      pointer=(this.length+(pointer+by))%this.length;
    }
    return this[pointer];
  }.bind(this);
  this.next=this.rotate.bind(this,1);
  this.prev=this.rotate.bind(this,-1);
  this.current=this.rotate.bind(this,0);
  this.__defineGetter__("index",function(){return pointer;});
}).extends(Array);

function createIdStructure(root,children){
	var chldrn = children || root.children;
  for (var i=0; i<chldrn.length; i++){
		if (chldrn[i].id != "") {
			root[chldrn[i].id.toString()] = chldrn[i];
			createIdStructure(chldrn[i]);
		}
    else {
			createIdStructure(root,chldrn[i].children);
		}
  }
}

function getOffset(element,side){
  var of=0,s="offset"+side.charAt(0).toUpperCase()+side.slice(1);
  for(var e = element;e!=document;e=e.parentNode){
    of+=e[s];
  }
  return of;
}

function element(desc){
  if (!desc) {throw new Error("Wrong argument")}
  var type = desc.match(/^[a-z][a-z0-9-]*/i);
  var classes = desc.match(/\.([a-z][a-z0-9_-]*)/ig) || [];
  var id = desc.match(/\#([a-z][a-z0-9_-]*)/i) || [];
  var element = document.createElement(type[0]);
  element.className = ((classes.length>0) ? (classes.join(" ")) : ("")).replace(/\./g,"");
  element.id = (id.length>0) ? (id[0].replace(/\#/,"")) : ("");
  return element;
}

var app = {
  gems: null,
  gem: {
    gallery: null
  }
};

function compileToggles(els){
  toA(els).forEach(function(e,i,a){
    e.toggles=toA(document.querySelectorAll("label[for="+e.id+"]")).map(function(e,i,a){
      if (e.parentNode.classList.contains("label")){
        e.addBCListener("uncheck",function(){
          if (this.parentNode.getAttribute("checked") == "true") this.click();
        });
      }
      return e.parentNode;
    });
    e.addEventListener("click", function(ev){
      this.toggles.forEach(function(e,i,a){
        e.setAttribute("checked",this.checked);
        if (!this.checked) e.parentNode.broadcast("uncheck");
      },this);
    }.bind(e));
  });
}

function setData(el,newdata){
  var i,s;
  for (i in newdata){
    el.setAttribute("data-"+i,newdata[i]);
  }
}


function gallery(root){
  if (!(root instanceof HTMLElement)) {
    throw new Error("gallery(): root is not an HTMLElement");
  }

  createIdStructure(root);

  var auto,pics=new Rotator(toA(root["pictures-view"].getElementsByClassName("picture"))),
      view = root.view,
      image = root.view.imageview,
      inbutton = root["zoom-in"],
      outbutton = root["zoom-out"];

	function setSlideshow(interval){
		if (!auto) auto = setInterval(function(){loadPic(pics.next().dataset)},interval || 10000);
		else {
			stopSlideshow(auto);
			setSlideshow(interval);
		}
	}

	function stopSlideshow(){
		if (auto) clearInterval(auto);
		auto = false;
	}

	function editmode(v){
		if (arguments.length) {
			if (arguments[0]) root.dataset.editmode = arguments[0];
			else delete root.dataset.editmode;
		}
		return root.dataset.editmode;
	}

  root["pictures-view"].addBCListener("redrawn",function(dataset){
    var id = dataset.id;
    var target = this.querySelector("img#"+id);
    setData(target,dataset);
    for (f in dataset){
      if (f != "id"){
        var it = this.querySelector("input#"+(id+"_"+f));
        if (it) it.setAttribute("value", dataset[f]);
      }
    }
  });

  view.__defineGetter__("pageTop",getOffset.bind(view,view,"Top"));
  view.__defineGetter__("pageLeft",getOffset.bind(view,view,"Left"));
  pics.forEach(function(e,i,a){e.parentNode.href="javascript: void(0);"});
  var _clk = 0, _clktimer;
	var _initpoint = false;
  var _movepoint = false;
  var _scalepoint = false;
  var fT,sT;

  function initclk(x,y){
		_initpoint = [x,y];
    _clk++;
		resetClkTimer();
		_clktimer = setTimeout(clearclk,150);
  }

	function resetClkTimer(){
		if (_clktimer) {
			clearTimeout(_clktimer);
			_clktimer = undefined;
		}
	}

  function clearclk(){
		_initpoint = false;
	  _clk = 0;
		resetClkTimer();
  }

  function clk(e){
    if (_initpoint) {
      if (Math.sqrt(Math.pow(_initpoint[0]-e.clientX,2)+Math.pow(_initpoint[1]-e.clientY,2))<5){
				resetClkTimer();
				_clktimer = setTimeout(function(){
					switch(_clk){
	          case 1:
	            editmode(!root.dataset.editmode);
	            break;
	          case 2:
	            center(e);
	            break;
	        }
	        clearclk();
				},150);
      }
			else clearclk();
    }
		else clearclk();
  }

  function stopmove(){_movepoint=fT=false}
  function stopscale(){_scalepoint=sT=false}

  function rawmoveby(mvect){
    var el=view;
    var x = el.scrollLeft += mvect[0];
    var y = el.scrollTop += mvect[1];
    rawmoveto(x,y);
  }

  function rawmoveto(x,y){
    var el=view;
    el.scrollLeft = x;
    el.scrollTop = y;
  }

  function rawmove(start,finish){
    var el = view, img = image;
    rawmoveby([start[0]-finish[0],start[1]-finish[1]]);
    img.dataset.x = el.scrollLeft;
    img.dataset.y = el.scrollTop;
    img.dispatchEvent(new CustomEvent("redraw",{detail:img.dataset,bubbles:true}));
  }

  function rawcenter(){
    var el = view;
    el.scrollLeft = (el.scrollWidth-el.clientWidth)/2;
    el.scrollTop = (el.scrollHeight-el.clientHeight)/2;
  }

  function move(point){
		if (!editmode()) return;
    if (_movepoint){
      rawmove(_movepoint,[point.clientX,point.clientY]);
    }
    _movepoint = [point.clientX,point.clientY];
  }

  function touchmv(te){
    te.preventDefault();
    var t=toA(te.changedTouches);
    var p1=t.find(function(e,i,a){return e.identifier===fT});
    var p2=t.find(function(e,i,a){return e.identifier===sT});
    if (p1) move(p1);
    if (p2) pane(p2);
  }

  function scale(f){
		if (!editmode()) return;
    var el = image;
    rawscale(f);
    el.dataset.scale *= f;
    el.dataset.x = el.parentNode.scrollLeft;
    el.dataset.y = el.parentNode.scrollTop;
    el.dispatchEvent(new CustomEvent("redraw",{detail:el.dataset, bubbles: true}));
  }

  function rawscale(f){
    var el = image,
        sx = el.clientWidth, sy = el.clientHeight;
    el.style.width = Math.round(sx*f).toString()+"px";
    el.style.height = Math.round(sy*f).toString()+"px";
    rawmoveby([(f-1)*sx/2,(f-1)*sy/2]);
  }

  function pane (point2){
		if (!editmode()) return;
    if (_scalepoint){
      var dx1=_movepoint[0]-_scalepoint[0],
          dx2=_movepoint[0]-point2.clientX,
          dy1=_movepoint[1]-_scalepoint[1],
          dy2=_movepoint[1]-point2.clientY;
      var f1 = Math.sqrt(Math.pow(dx1,2)+Math.pow(dy1,2)),
          f2 = Math.sqrt(Math.pow(dx2,2)+Math.pow(dy2,2));

      scale(f2/f1);
    }
    _scalepoint = [point2.clientX,point2.clientY];
  }

  function center(point){
    if (!editmode()) return;
		var el = view;
    if (!point) {
      rawcenter();
    }
    else {
      rawmove([point.clientX-el.pageLeft,point.clientY-el.pageTop],[el.clientWidth/2,el.clientHeight/2]);
    }
  }

  var setPic = (function PicUpdater(){
		var src = "",
		dataset = {};
		root.shade.addEventListener("animationend",function(e){
			if (e.target == root.shade && e.animationName == "shade") {
				if ((src !== "") && (dataset !== {})) {
					image.style.width="auto";
			    image.style.height="auto";
			    setData(image,dataset);
					image.src = src;
					PositionImage();
				}
			}
		}, false);
		return function(s,d){
			src = s;
			dataset = d;
		}
  })();

  function loadPic(picsrc){
		root.setAttribute("loaded",false);
		nl_ajax({
		  method: "GET",
		  url: picsrc.src,
		  type: "blob",
		  resulthandler: function(xhr){
				if (xhr.status == 200) {
					if (image.src != "") URL.revokeObjectURL(image.src);
					setPic(URL.createObjectURL(xhr.response), picsrc);
				}
			},
			progresshandler: document.getElementById("logo_progress"),
		});
  }

	function PositionImage(e){
		var s = image.dataset.scale,
				x = image.dataset.x,
				y = image.dataset.y;

		if ((image.clientWidth/image.clientHeight)>=(view.clientWidth/view.clientHeight)) {
			image.style.height = view.clientHeight.toString()+"px";
		}
		else{
			image.style.width = view.clientWidth.toString()+"px";
		}
		rawcenter();

		rawscale(s);
		rawmoveto(x,y);

		root.setAttribute("loaded",true);
	}

  image.addEventListener("load", PositionImage);

  //view.addEventListener("dblclick",center);

  inbutton.addEventListener("click",function(){
    scale(1.1);
  });

  outbutton.addEventListener("click",function(){
    scale(0.9);
  });

  view.addEventListener("mousedown",function(e){
    if (e.target===image) {
			initclk(e.clientX,e.clientY);
			stopSlideshow();
      view.addEventListener("mousemove",move);
    }
  });

  view.addEventListener("mouseup",stopmove);
  view.addEventListener("mouseup",function(e){
    view.removeEventListener("mousemove",move);
    clk(e);
  });

  view.addEventListener("mouseleave",stopmove);
  view.addEventListener("mouseleave",function(e){
    view.removeEventListener("mousemove",move);
  });

  view.addEventListener("touchstart",function(e){
    if (e.target===image) {
			stopSlideshow();
      e.preventDefault();
      if (e.targetTouches.length < 3){
        if (e.targetTouches.length == 1){
          fT = e.targetTouches[0].identifier;
          initclk(e.targetTouches[0].clientX,e.targetTouches[0].clientY);
        }
        else sT = e.targetTouches[1].identifier;
      }
    }
  });

	view.addEventListener("touchmove",touchmv);

  view.addEventListener("touchend",function(e){
    if (e.target===image) {
      e.preventDefault();
      var t=toA(e.changedTouches);
      var p1=t.find(function(e,i,a){return e.identifier===fT});
      var p2=t.find(function(e,i,a){return e.identifier===sT});
      if (p1) stopmove();
      if (p2) stopscale();
      else if (p1) clk(p1);
    }
  });

  root["pictures-view"].addEventListener("click",function(e){
    if (e.target.classList.contains("picture")){
      stopSlideshow();
      loadPic(e.target.dataset); // e.target.getAttribute("data-src");
    }
  });

  root.addEventListener("redraw", function(e){
    root["pictures-view"].broadcast("redrawn",e.detail);
  });

	root.addEventListener("resize", PositionImage);

  root.pics = pics;
  view.classList.remove("scrollable");
  inbutton.classList.remove("hidden");
  outbutton.classList.remove("hidden");
  if (pics.length > 0){
    loadPic(pics.current().dataset);
    setSlideshow(10000);
  }
  return root;
}

document.defaultView.updateAvatar = function(input){
  var img = document.getElementById("avatar");
  
  if (img.hasAttribute("default")) {
    if (input.checkValidity()) {
      img.src = "https://secure.gravatar.com/avatar/" + md5(input.value.trim().toLowerCase()) + "?s=80&d=identicon";
    }
    else {
      img.src = APP_ICON;
    }
    
  }
}

//              end library

//              ENTRY POINT

function main(){
  document.getElementById("logo_progress").max=100;
  document.getElementById("logo_progress").value=0;
  compileToggles(document.querySelectorAll("input[type=checkbox].invisible"));
}


window.addEventListener("load",main);

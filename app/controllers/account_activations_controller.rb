class AccountActivationsController < ApplicationController

  def edit
    user = User.unactive.find_by(email: params[:email])
    if user && user.authenticated?(params[:id],"activation")
      user.activate
      flash[:success] = "Account activated! You can now log in using your email and password."
      redirect_to login_url
    else
      if user
        user.destroy
      end
      flash[:danger] = "Invalid activation!"
      redirect_to root_url
    end
  end
end

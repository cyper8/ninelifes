class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.

  protect_from_forgery with: :exception
  include SessionsHelper

  def home
    @prods_per_page = 10
    if logged_in? && section = home_params.delete(:section)
      @section = case section
      when "my"
        @items = @current_user.prods.includes({:tags => [:similar]}, :pictures, :owner)
        "my"
      when "friends"
        @items = @current_user
          .contacts
          .includes(:prods, :comments, :bookmarks)
          .order(:last_visit)
        "friends"
      else
        @items = @current_user.favourites.includes({:tags => [:similar]}, :pictures, :owner)
        "favourites"
      end
    else
      @items = Prod.joins(:owner).includes({:tags => [:similar]}, :pictures, :owner)
    end
    if @filter = home_params.delete(:filter)
      @items = @items.search(@filter) unless @filter.empty?
    end
    @max_page = (@items.count/@prods_per_page.to_f).ceil-1
    @page = (home_params.delete(:page)||0).to_i.put_within(0,@max_page)
    @items = @items.limit(@prods_per_page).offset(@page*@prods_per_page)
    render 'home'
  end

  def search
    if @filter = search_params[:filter]
      unless @filter.empty?
        @prods = Prod.search(@filter)
        @tags = Tag.includes(:similar).search(@filter).list.load
        if @current_user = current_user
          @users = @current_user.contacts.search(@filter)
        else
          @users = nil
        end
      end
    end
  end

  private
    def home_params
      params.permit(:section, :page, :filter)
    end

    def operation_rejected
      flash[:warning] = "Operation rejected."
      redirect_to(root_url)
    end

    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    def mark_visit
      if logged_in?
        @last_visit = visit(@current_user)
      end
    end

    def is_admin_user?
      current_user.admin?
    end

    def admin_user
      operation_rejected unless is_admin_user?
    end

    def visit(user)
      now = DateTime.now
      res = now
      if user
        if user.last_visit?
          res = user.last_visit
        end
        user.update(last_visit: now)
      end
      return res
    end

end

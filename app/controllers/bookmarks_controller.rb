class BookmarksController < ApplicationController
  before_action :logged_in_user
  before_action :on_prod
  before_action :is_not_owner

  def create
    @current_user.bookmark(@prod)
    bookmarks_reset
    respond_to do |format|
      format.html { redirect_to @prod }
      format.js { render "bookmarks/_refresh_bookmark.js.erb" }
    end
  end

  def destroy
    @current_user.unbookmark(@prod)
    bookmarks_reset
    respond_to do |format|
      format.html { redirect_to @prod }
      format.js { render "bookmarks/_refresh_bookmark.js.erb" }
    end
  end

  private
    def on_prod
      unless @prod = Prod.includes(:owner).find_by(id: params[:prod_id])
        head :no_content
        return false
      end
    end
  
    def is_not_owner
      if @prod.owner == @current_user
        head :no_content
        return false
      end
    end
  
    def bookmarks_reset
      @prod.bookmarks.reset
      @prod.bookmarked_by.reset
      @current_user.bookmarks.reset
    end
end

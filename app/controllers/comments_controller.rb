class CommentsController < ApplicationController
  before_action :logged_in_user, except: [:show, :replies]
  before_action :on_comment, except: [:new, :create]
  before_action :is_author, only: [:edit, :update]
  before_action :correct_user, only: [:destroy]

  def show
    @current_user = current_user
    id = @comment.id
    comment = @comment
    if comment.commentable_type == "Comment"
      until comment.commentable_type != "Comment" do
        comment = comment.commentable
      end
    end
    case comment.commentable_type
    when "MessageGroup"
      redirect_to message_group_url(comment.commentable_id)+"#comment-#{id}"
    else
      redirect_to comments_on_prod_path(comment.commentable)+"?thread=#{comment.id}#comment-#{id}"
    end
  end

  def edit
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update
    if @comment.update(new_comment_params)
      if comment_pics = new_comment_pics[:pics]
        comment_pics.each do |pic|
          @comment.pictures.create!(picture: pic)
        end
        @comment.pictures.reset
      end
    else
      flash[:danger] = "Failed to edit comment"
      #@feed_items = []
      redirect_to url_for(:back) || root_url
    end
    respond_to do |format|
      format.html { redirect_back_or(:root) }
      format.js
    end
  end

  def create
    p = new_comment_params
    @comment = @current_user.comments.build(p)
    @target = case p[:commentable_type]
      when "Prod"
        assosiation = @current_user.discussions
        Prod.find_by(id: p[:commentable_id])
      when "Comment"
        assosiation = @current_user.replies
        Comment.find_by(id: p[:commentable_id])
      when "MessageGroup"
        assosiation = @current_user.messages
        MessageGroup.find_by(id: p[:commentable_id])
      else
        nil
    end
    if @comment.save
      comment_pics = new_comment_pics[:pics]
      comment_pics.each do |pic|
        @comment.pictures.create!(picture: pic)
      end unless comment_pics.nil?
      assosiation.reset
    else
      flash[:danger] = "Failed to post comment"
      #@feed_items = []
      redirect_to url_for(:back) || root_url
    end
    respond_to do |format|
      format.html { redirect_to url_for(@target)+"#comment-#{@comment.id}" }
      format.js
    end
  end

  def destroy
    @comment.destroy
    respond_to do |type|
      type.html { redirect_back_or(:root) }
      type.js { render js: "$('div.comment#comment-#{@comment.id}').remove();" }
    end
  end

  def comment
    @target = @comment
    if @new_comment = @target.replies.build(author: current_user)
      @target.replies.reset
    end
    respond_to do |format|
      format.html
      format.js
    end
  end

  def replies
    @current_user = current_user
    @target = @comment
    @replies = @target.replies.reload
    per_page = 3
    @max_page = ((@replies.count/per_page.to_f).ceil)-1
    @page = (params[:page]||0).to_i.put_within(0,@max_page)
    @comments = @replies.includes(:pictures, :author)
      .reverse_order
      .offset(@page*per_page)
      .limit(per_page)
      .to_a
      .reverse
    respond_to do |format|
      format.js
      format.html { render layout: "application" }
    end
  end

  private
# ---------- params filtering ---------------------

    def new_comment_params
      params.require(:comment).permit(:content,:commentable_id,:commentable_type)
    end

    def new_comment_pics
      params.require(:comment).permit( {:pics => []} )
    end

# ---------- before_action filters -----------------

    def on_comment
      unless @comment = Comment.includes(:author, :commentable, :pictures).find_by(id: params[:id])
        flash[:danger] = "Comment does not exist."
        redirect_back_or root_url
      end
    end

    def is_author # used after on_comment
      unless @comment.author_is?(@current_user)
        flash[:danger] = "Only author can edit a comment."
        redirect_back_or root_url
      end
    end

    def correct_user # used after on_comment
      unless (( @comment.author_is?(@current_user) ) || @current_user.admin?)
        flash[:danger] = "Only author or admin can delete a comment."
        redirect_back_or root_url
      end
    end
end

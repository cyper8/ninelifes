class MessageGroupsController < ApplicationController
  before_action :logged_in_user # sets @current_user
  before_action :message_groups_list # sets @message_groups
  before_action :on_message_group, except: [:index, :new, :create] # sets @message_group
  before_action :mark_participants_visit, only: [:show, :talk]
  before_action :is_moderator, only: [:edit, :update, :destroy]
  before_action :check_participants, only: [:create, :update]
  before_action :user_can_talk, only: [:show, :talk, :leave, :messages]

  def index
    # should render whole messenger page
  end

  def new
    @message_group = @current_user.message_groups.new
    @contacts = @current_user.contacts
    # render 'form'
    render 'index'
  end

  def edit
    # @message_group
    @contacts = @current_user.contacts
    # render 'form'
    render 'index'
  end

  def create
    name = mg_params[:name] || ""
    @message_group = @current_user.message_groups.build(name: name)
    if @message_group.save
      @participants = @message_group.participants
        .build(mg_params[:participants].uniq.map { |p| { user_id: p } })
      @message_group.participants<< @participants
      # should render messenger page with current mg selected
      render 'index'
    else
      flash[:danger] = "Failed to save message group."
      redirect_to edit_message_group_path(@message_group)
    end
  end

  def update
    @message_group.name = mg_params[:name]
    @participants = @message_group.participants
      .build(mg_params[:participants].uniq.map { |p| { user_id: p } })
    @message_group.participants = @participants
    if @message_group.save
      # should render messenger page with current mg selected
      render 'index'
    else
      flash[:danger] = "Failed to update message group."
      redirect_to edit_message_group_path(@message_group)
    end
  end

  def show
    # should render messenger page with current mg selected
    render 'index'
  end

  def talk
    @message = @message_group.messages.create!(author: @current_user, content: message_params[:content])
    if @message.save
      message_pics = message_pics_params[:pics]
      message_pics.each do |pic|
        @message.pictures.create!(picture: pic)
      end unless message_pics.nil?
      @message_group.messages.reload
      @message_group.conversants.includes(:subscription).each do |user|
        if user != @current_user
          user.notify(message_group_url(@message_group),@message_group.name,@message)
        end
      end
    else
      flash[:danger] = "Failed to send message"
      redirect_to url_for(:back) || root_url
    end
    # should render messenger page with current mg selected
    respond_to do |via|
      via.html { render 'index' }
      via.js
    end
  end

  def leave
    if @message_group.moderator == @current_user
      flash[:danger] = "Moderator cannot leave the group"
      redirect_to @message_group
    end
    if @participant = @message_group.participants.where(user_id: @current_user.id).first
      @message_group.participants.destroy(@participant)
      @current_user.chats.reset
      @message_groups.reload
      @message_group = nil
    end
    # should render whole messenger page
    render 'index'
  end

  def messages
    @per_page = MessageGroup.msgs_per_page
    @max_page = ((@message_group.messages.count/@per_page.to_f).ceil)-1
    @page = params[:page].to_i.between?(0,@max_page) ? params[:page].to_i : nil
    if @page.nil?
      head :no_content
    else
      @messages = @message_group.messages.offset(@page*@per_page).limit(@per_page).to_a.reverse
      respond_to do |via|
        via.js
        via.html { render 'index' }
      end
    end
  end

  def destroy
    @message_group.destroy!
    @message_group = nil
    @message_groups.reload
    # should render whole messenger page
    redirect_back_or message_groups_path
  end

  private
# --------------- params filters ----------------

    def mg_params
      params.require(:message_group).permit(
        :name,
        :participants => []
      )
    end

    def message_params
      params.require(:comment).permit(:content)
    end

    def message_pics_params
      params.require(:comment).permit( {:pics => []} )
    end

# ------------- before_action filters --------------------

    def check_participants
      unless mg_params[:participants] && (mg_params[:participants].length > 0) && !(mg_params[:participants].include?(@current_user.id))
        flash[:danger] = "Group cannot contain zero recipients."
        redirect_to edit_message_group_path(@message_group)
      end
    end

    def message_groups_list
      @message_groups = @current_user.chats.includes(:moderator, :conversants)
    end

    def on_message_group
      unless @message_group = @message_groups.includes(:participants, :messages).find_by(id: params[:id])
        flash[:danger] = "No such a group or you're not a participant."
        redirect_to request.referer || message_groups_path
      end
    end

    def is_moderator
      unless @message_group.moderator == @current_user
        flash[:danger] = "You have no permissions to edit this group"
        redirect_to request.referer || message_groups_path
      end
    end
    
    def mark_participants_visit
      @last_visit = @message_group.participants.find_by(user: @current_user).mark_visit
    end

    def user_can_talk
      unless (@message_group.moderator == @current_user) || (@message_group.conversants.include?(@current_user))
        flash[:danger] = "You have no permissions to text to this group"
        redirect_to request.referer || message_groups_path
      end
    end

end

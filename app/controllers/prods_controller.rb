class ProdsController < ApplicationController
  before_action :logged_in_user, except: [:show, :index, :comments]
  before_action :list_prods, only: [:show, :comments]
  before_action :set_prod, only: [:edit, :update, :show, :comments]
  before_action :mark_visit, only: [:show, :comments]
  before_action :is_owner, only: [:edit, :update]

  def index
    redirect_to prod_path(id: 1)
  end

  def new
    @prod = @current_user.prods.new(title: "New Gem")
    render 'edit'
  end

  def create
    unsaved_pics = []
    @prod = @current_user.prods.new(prod_parameters)
    if @prod.save
      if @prod_new_pics = prod_pics_params[:add_pictures]
        @prod_new_pics[:pictures].each_with_index do |pic,index|
          p = { picture: pic }
          if @prod_new_pics[:new_params]["#{index}"]
            p[:x] = @prod_new_pics[:new_params]["#{index}"][:x]
            p[:y] = @prod_new_pics[:new_params]["#{index}"][:y]
            p[:scale] = @prod_new_pics[:new_params]["#{index}"][:scale]
          end
          unsaved_pics.push(pic[:filename]) unless @prod.pictures.create!(p)
        end
      end
      if unsaved_pics.length != 0
        flash[:danger] = "Gem successfully saved, but those pictures were not: #{unsaved_pics.join(',')}"
        redirect_to edit_prod(@prod) and return
      end
      flash[:success] = "New prod saved!"
      redirect_to @prod and return
    else
      render 'edit'
    end
  end

  def show
    if @prod.pictures.any?
      if params[:pic].nil?
        @pic = @prod.pictures.first
      else
        @pic = @prod.pictures.find(params[:pic])
      end
    end
    @message_groups = @current_user.chats.reload.includes(:moderator, :conversants, :messages) unless @current_user.nil?
    respond_to do |format|
      format.html
      format.js
    end
  end

  def edit
    respond_to do |format|
      format.html
      format.js
    end
  end

  def update
    unsaved_pics = []
    if @prod.update(prod_parameters)
      if @prod_new_pics = prod_pics_params[:add_pictures]
        @prod_new_pics[:pictures].each_with_index do |pic,index|
          p = { picture: pic }
          if @prod_new_pics[:new_params]["#{index}"]
            p[:x] = @prod_new_pics[:new_params]["#{index}"][:x]
            p[:y] = @prod_new_pics[:new_params]["#{index}"][:y]
            p[:scale] = @prod_new_pics[:new_params]["#{index}"][:scale]
          end
          unsaved_pics.push(pic[:filename]) unless @prod.pictures.create!(p)
        end
      end
      if @prod_pics = prod_pics_params[:pictures_attributes]
        @prod_pics.each do |k,pic|
          newpic = @prod.pictures.find(pic[:id])
          if pic[:remove_picture] == "1"
            newpic.destroy!
          else
            unsaved_pics.push(newpic.picture_identifier) unless newpic.update(pic)
          end
        end
      end
    else
      flash[:danger] = "Failed to save Gem"
      render 'edit' and return
    end
    if unsaved_pics.length > 0
      flash[:danger] = "Failed to save associated pictures: #{unsaved_pics.join(',')}"
      render 'edit' and return
    end
    render 'show'
  end

  def destroy
    if @prod = Prod.find(params[:id] || 1) && @current_user == @prod.owner
      name = @prod.title
      if @prod.destroy
        flash[:success] = "#{name} was successfully removed."
        redirect_to root_url and return
      end
    end
    flash[:danger] = "Gem removal failed."
    redirect_back_or root_url
  end

  def comments
    if page = prod_comments_params[:page]
      per_page = 3
      @max_page = ((@prod.comments.count/per_page.to_f).ceil)-1
      @page = page.to_i.between?(0,@max_page) ? page.to_i : nil
      if !@page.nil?
        @comments = @prod.comments.reverse_order.offset(@page*per_page).limit(per_page).to_a.reverse
      else
        head :no_content and return
      end
    elsif thread = prod_comments_params[:thread]
      if comment = @prod.comments.find_by(id: thread)
        @thread = true
        @comment = comment
        render 'show' and return
      else
        head :no_content and return
      end
    else
      head :no_content and return
    end
    respond_to do |format|
      format.js
      format.html { render "show" }
    end
  end

  private
# ---------- params filtering -------------
    def prod_parameters
      params.require(:prod).permit(
        :title,
        :subtitle,
        :description,
      )
    end

    def prod_comments_params
      params.permit(:page, :thread)
    end

    def prod_pics_params
      params.require(:prod).permit(
        :pictures_attributes => [
          :remove_picture,
          :x,
          :y,
          :scale,
          :id
        ],
        :add_pictures => [
          {:pictures => []},
          :new_params => [
            :x,
            :y,
            :scale
          ]
        ]
      )
    end

# --------------- before_action filters -------------------

    def list_prods
      @prods = Prod.joins(:owner).includes(:pictures, :comments)
    end

    def set_prod
      @prod = Prod.includes(:comments, { :bookmarked_by => [:avatarfile] }, {:tags => [:similar]}, :pictures, :owner).find_by(id: params[:id] || 1)
    end

    def is_owner
      unless @current_user == @prod.owner
        flash[:danger] = "Only owner can edit a gem."
        redirect_back_or @prod
      end
    end


end

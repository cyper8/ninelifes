class SessionsController < ApplicationController

  def new
    session[:forwarding_url] = request.referrer if session[:forwarding_url].nil?
  end

  def create
    usr = user_params
    if usr[:provider] == 'email'
      user = User.find_by(provider: usr[:provider], uid: usr[:uid])
      if user && user.authenticate(usr[:password])
        log_in user
        usr[:remember_me] == '1' ? remember(user) : forget(user)
      else
        flash.now[:danger] = 'Invalid email/password combination or not activated'
        render 'new' and return
      end
    else
      begin
        @user = User.from_omniauth(usr)
        log_in @user
        remember(@user)
        flash[:success] = "Welcome, #{@user.name}!"
      rescue
        flash[:danger] = "There was an error while trying to authenticate you..."
      end
    end
    redirect_back_or(root_url)
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end

  def auth_failure
    redirect_to root_url
  end

  private
    def user_params
      usr = params[:session].nil? ? request.env['omniauth.auth'] : params[:session]
      if usr[:provider] == "email"
        usr[:uid] = usr[:email]
        return usr
      else
        return {
          uid: usr['uid'],
          provider: usr['provider'],
          name: usr['info']['name'],
          email: (usr['info']['email'] || "").downcase,
          avatar: usr['info']['image'],
          usrdata: usr['extra']['raw_info']['profile'] || usr['extra']['raw_info']['link']
        }
      end
    end
end

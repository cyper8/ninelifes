class TagsController < ApplicationController
  before_action :logged_in_user, except: [:index, :show]
  #before_action :admin_user, only: [:create, :destroy]
  before_action :set_target, only: [:create]

  def create       # tag given prod
    if @prod.nil?
      flash[:danger]="No such a product. Are you hacking me?"
      redirect_to :root and return
    end
    if params[:tag][:name].nil?
      flash[:danger]="Provide tag name, please."
      redirect_to @prod and return
    end
    unless @prod.tags.where(name: params[:tag][:name]).exists?
      @tag = @prod.tags.create(name: params[:tag][:name])
    else
      redirect_to @prod and return
    end
    respond_to do |format|
      format.html { redirect_to @prod }
      format.js
    end
  end

  def index     # return list of all tags by name with ability to show prods behind it
    @tags = Tag.list.includes(:similar)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def show       # show prods that have this tag
    if params[:name].nil?
      redirect_to :tags_url
    else
      @prods_per_page = 10
      @tagname = params[:name]
      @items = Prod.where(id: Tag.select(:taggable_id).where(name: @tagname, taggable_type: "Prod")).includes(:owner, {:tags => [:similar]}, :pictures)
      @max_page = (@items.count/@prods_per_page.to_f).ceil-1
      @page = (params[:page]||0).to_i.put_within(0,@max_page)
      @items = @items.limit(@prods_per_page).offset(@page*@prods_per_page)
    end
  end

  def destroy      # untag prod
    if @tag = Tag.find_by(id: params[:id])
      if @tag.taggable_type == "Prod" && @tag.taggable.owner == @current_user
        @tag.destroy
      else
        flash[:danger] = "You are not allowed to untag this since you're not an owner."
        redirect_to @tag.taggable
      end
    end
    respond_to do |format|
      format.html { redirect_back_or(:root) }
      format.js { render js: "$('.tag').remove('#id#{params[:id]}');" }
    end
  end

  private

    def set_target
      if @prod = Prod.includes(:tags).find_by(id: params[:prod_id], owner: @current_user)
        
      else
        redirect_back_or(:root)
      end
    end
    
end

class UsersController < ApplicationController
  skip_before_action :verify_authenticity_token, :only => [:subscribe] # skip csrf token verification from sw-generated requests
  before_action :logged_in_user, except: [:new, :create] # @current_user is set to logged in user
  before_action :on_user,        except: [:new, :create, :index] # @user is set to an existant user, on which an action is about to be performed
  before_action :is_active, except: [:new, :create, :index, :show, :edit]
  before_action :correct_user,   only: [:edit, :update, :destroy]
  before_action :is_me, only: [:subscribe]
  before_action :is_aquainted, only: [:text]

  def new
    @user = User.new
  end

  def index
    @users = @current_user.contacts.includes(:avatarfile, :prods, :comments, :bookmarks)
  end

  def create
    parameters = user_params
    if parameters[:provider] == 'email'
      parameters[:uid] = parameters[:email]
    end
    @user = User.unactive.new(parameters)
    if @user.save
      @user.create_activation_digest
      @user.send_activation_email
      flash[:warning] = "Please check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end

  def show
    if !@user.activated?
      redirect_to edit_user_path(@user) and return
    end
    @filter = ["comments","bookmarks"]
    @actions_offset = 0
    if filter = user_actions_filter[:actions]
      @activity_filter = case filter
      when "bookmarks"
        @filter = [filter]
        filter.capitalize
      when "comments"
        @filter = [filter]
        filter.capitalize
      else
        nil
      end
    end
    if page = user_actions_filter[:page]
      @page = page.to_i
      @actions_offset = @page*User.actions_per_page
    end
    @user_actions = @user.feed(scope_names: @filter,offset: @actions_offset,asc: false).to_hash
  end

  def edit
    if !@user.activated?
      @user.create_activation_digest
    end
  end

  def update
    if !current_user?(@user) && is_admin_user?
      # admin edits other users profile:
      #   can edit only security params
      security_edit
    else
      # ordinary user or admin edits himself:
      #   can edit name/passwd/avatar
      basic_edit
    end
  end

  def text
    mg = @current_user.message_groups.create(name: @user.name)
    mg.participants.create(user: @user)
    if mg.save
      redirect_to mg
    else
      flash[:danger] = "Failed to create chat"
      redirect_back_or root_url
    end
  end

  def subscribe
    subscription = subscription_params
    if @user.subscription.nil?
      @subscription = @user.build_subscription
    else
      @subscription = @user.subscription
    end
    @subscription.endpoint = subscription[:endpoint]
    @subscription.auth = subscription[:keys][:auth]
    @subscription.key = subscription[:keys][:p256dh]
    @subscription.save
    head :no_content
  end

  def destroy
    if (current_user?(@user) && !@current_user.admin?) || (!current_user?(@user) && @current_user.admin?) # admin cannot delete himself!
      log_out if current_user?(@user)
      name = @user.name
      @user.destroy
      flash[:success] = "User #{name} deleted!"
      redirect_to root_url
    else
      flash[:danger] = "Cannot delete yourself!"
      redirect_back_or @user
    end
  end

  private
# ---------- params filtering ---------------

    def user_params
      params.require(:user).permit(:provider, :uid, :email, :name, :password,
                                   :password_confirmation, :avatar, :remove_avatar)
    end

    def user_params_for_admin
      params.require(:user).permit(:password, :password_confirmation, :activated, :admin)
    end

    def user_actions_filter
      params.permit(:id, :actions, :page)
    end

    def subscription_params
      params.require(:subscription).permit(:endpoint,:expirationTime,:keys => [:p256dh,:auth])
    end

# ----------------- before_action filters ------------------

    def on_user
      from_all_users = if @current_user.admin?
        User.unscope(where: :activated)
      else
        User
      end
      unless @user = from_all_users.includes(:avatarfile, :prods, :favourites).find_by(id: params[:id])
        flash[:danger] = "You need to login to access this page."
        redirect_to login_url
      end
    end

    def is_me
      operation_rejected unless @user == @current_user
    end

    def correct_user
      if !is_admin_user?
        is_me
      end
    end

    def is_active
      unless @user.activated?
        redirect_to root_url and return
      end
    end

    def basic_edit
      parameters = user_params
      if parameters[:remove_avatar] == "1"
        @user.remove_avatar
      end
      if !parameters[:avatar].nil?
        if @user.avatarfile.nil?
          newavatar = @user.build_avatarfile(picture: parameters[:avatar])
          newavatar.picture.resize_to_limit(128,128)
          newavatar.save!
        else
          @user.avatarfile.picture = parameters[:avatar]
          @user.avatarfile(true).picture.resize_to_limit(128,128).save!
        end
        parameters[:avatar] = @user.avatarfile(true).picture_url
      end
      if @user.update_attributes(parameters)
        flash[:success] = "Profile updated"
        redirect_to @user
      else
        flash.now[:danger] = "Provided information doesn't meet requirements."
        render 'edit'
      end
    end

    def security_edit
      parameters = user_params_for_admin
      if parameters.delete(:activated) == "false"
        @user.disable
      end
      if @user.update_attributes(parameters)
        flash[:success] = "Profile updated"
        redirect_to @user
      else
        flash.now[:danger] = "Provided information doesn't meet requirements."
        render 'edit'
      end
    end

    def is_aquainted
      unless @current_user.contacts.include?(@user)
        redirect_to request.referer, flash: { danger: "Cannot text to someone you're not aquainted with.\n You need to bookmark or comment on #{@user.name}'s creations first." }
      end
    end

end

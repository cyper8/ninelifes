module ApplicationHelper
    # Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Ninelifes - Homeplace for Creative Person"
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end

  def section_nav_tag (label, icon: nil, url: nil, selected: false, inactive: false)
    tagopts = { class: "menuitem", alt: label }
    tagopts["selected"] = "selected" if selected
    tagopts["inactive"] = "inactive" if inactive
    item = ""
    unless icon.nil?
      item += content_tag :div, class: "icon" do
        render file: icon
      end
    end
    item += content_tag :span, label, class: "item-label"
    if !selected && !inactive && !url.nil?
      item = link_to item.html_safe, url
    end
    if block_given?
      item += yield
    end
    content_tag :div, tagopts do
      item.html_safe
    end
  end

end

module BookmarksHelper
  def get_my_bookmark(prod)
    if logged_in?
      if prod.any?
        prod.bookmarks.where(user_id: current_user.id)
      end
    end
  end
end

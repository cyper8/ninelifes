module ProdHelper
  
  class HTMLWithPants < Redcarpet::Render::HTML
    include Redcarpet::Render::SmartyPants
  end
  
  def markdown(content)
    @markdown ||= Redcarpet::Markdown.new(HTMLWithPants, {
      autolink: true,
      fenced_code_blocks: true,
      underline: true,
      highlight: true,
      footnotes: true,
      space_after_headers: true
    })
    @markdown.render(content)
  end
end

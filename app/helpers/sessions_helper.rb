module SessionsHelper

  #Logs in the given user
  def log_in(user)
    tmp_session = session.dup
    request.reset_session
    session.merge! tmp_session
    session[:user_id] = user.id
  end

  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end

  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end

  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
      if user && user.authenticated?(cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  def current_user?(user)
    if @current_user
      user == @current_user
    else
      user == current_user
    end
  end

  def logged_in?
    !current_user.nil?
  end

  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  def store_location
    session[:forwarding_url] = request.url if request.get?
  end

  def google_signin_button
    link_to '/auth/google' do
      image_tag "btn_google_signin_light_normal_web.png", class: "social-login googleplus-login", id: "google"
    end
  end

  def fb_signin_button
    link_to '/auth/facebook' do
      image_tag "btn_fb_signin.png", class: "social-login", id: "facebook"
    end
  end
end

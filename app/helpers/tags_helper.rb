module TagsHelper
  def get_tags(prod)
    if prod.nil?
      Tag.connection.select_all("SELECT DISTINCT name, count(name) as count \
                                 FROM Tags \
                                 GROUP BY Tags.name;").to_hash
    else
      Tag.connection.select_all("SELECT a.name, a.id, b.count \
        FROM (\
          SELECT name, id \
          FROM Tags \
          WHERE taggable_type = 'Prod' AND taggable_id = #{prod.id}\
        ) AS a \
        INNER JOIN (\
          SELECT name, count(name) AS count \
          FROM Tags \
          WHERE taggable_type = 'Prod' \
          GROUP BY name\
        ) AS b \
        ON a.name = b.name;").to_hash
    end
  end
end

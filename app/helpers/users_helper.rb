module UsersHelper

  def get_avatar_for (user)
    if user.avatarfile.nil?
      if user.avatar.nil?
        default = true
        if !user.email.nil?
          avatar_src = "https://secure.gravatar.com/avatar/#{Digest::MD5::hexdigest(user.email.downcase)}?d=identicon"
        else
          avatar_src = asset_path "nl-icon-96x96.png"
        end
      else
        avatar_src = user.avatar
      end
    else
      avatar_src = user.avatarfile.picture
    end
    image_tag(avatar_src, class: "avatar raised", id: "avatar", alt: "avatar", default: !!(default))
  end
end

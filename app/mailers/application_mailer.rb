class ApplicationMailer < ActionMailer::Base
  default from: "ninelifes@ya64.uk"
  layout 'mailer'
end

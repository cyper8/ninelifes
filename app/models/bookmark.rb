class Bookmark < ActiveRecord::Base
  belongs_to :user, inverse_of: :bookmarks
  belongs_to :prod, inverse_of: :bookmarks
  validates :user_id, presence: true
  validates :prod_id, presence: true
  default_scope -> { order(:created_at) }
  scope :created_after, ->(time) { where("created_at >= ?", time) }
end

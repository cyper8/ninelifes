class Comment < ActiveRecord::Base
  belongs_to :author, class_name: "User", foreign_key: "author_id"
  belongs_to :commentable, polymorphic: true
  has_many :replies,  as: :commentable,
                      class_name: "Comment",
                      foreign_key: "commentable_id",
                      inverse_of: :commentable,
                      dependent: :destroy
  has_many :pictures, as: :pictureowner, dependent: :destroy
  default_scope -> { order(:created_at) }
  validates :content, presence: true, length: { maximum: 255 }
  validates :author, presence: true
  scope :created_after, ->(time) { where("created_at >= ?", time) }

  def author_is?(user)
    self.author_id == user.id
  end

end

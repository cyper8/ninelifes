class Message < Comment
  belongs_to :message_group, foreign_key: "commentable_id"
  has_many :addressees, through: :message_group, source: :conversants

  validates :message_group, presence: true
  validate :if_author_belongs_to_group

  default_scope -> { unscope(:order).order(created_at: :desc) }

  def model_name
    Comment.model_name
  end

  private
    def if_author_belongs_to_group
      unless self.message_group.conversants.include?(self.author) then
        errors.add(:author, " has no right to message in this group")
      end
    end
end

class MessageGroup < ActiveRecord::Base
  belongs_to :moderator, class_name: "User"
  has_many :participants, class_name: "MgParticipant", dependent: :destroy
  has_many :conversants, through: :participants, source: :user
  has_many :messages, as: :commentable, dependent: :destroy

  validates :name, length: {maximum: 255}
  validates :moderator_id, presence: true
  validates_associated :participants

  default_scope -> { order(updated_at: :desc) }

  @@msgs_per_page = 5

  def MessageGroup.msgs_per_page
    @@msgs_per_page
  end

  after_save do
    unless self.conversants.include?(self.moderator)
      self.participants.create!(user: self.moderator)
    end
  end

  def new_messages_for(time)
    self.messages.created_after(time)
  end
end

class MgParticipant < ActiveRecord::Base
  belongs_to :message_group
  belongs_to :user
  has_many :messages, through: :user

  validates :message_group_id, presence: true
  validate :user_is_an_aquaintance

  def user_is_an_aquaintance
    unless message_group.moderator_id == user_id || message_group.moderator.contacts.include?(user)
      errors.add(:user, "not in the contacts list")
    end
  end

  after_destroy do
    group = self.message_group.participants
    if group.empty? || (group.length == 1 && group.include?(:user))
      group.destroy
    end
  end
  
  def unread
    self.message_group.messages.created_after(self.updated_at)
  end
  
  def mark_visit
    last = self.updated_at
    self.update(updated_at: DateTime.now)
    return last
  end

end

class Picture < ActiveRecord::Base
  belongs_to :avatarable, polymorphic: true
  belongs_to :pictureowner, polymorphic: true
  default_scope -> { order(created_at: :asc) }
  mount_uploader :picture, PictureUploader

  validates :x, numericality: { only_integer: true }
  validates :y, numericality: { only_integer: true }
  validates :scale, numericality: true
  validate :picture_size

  after_commit :remove_picfile, on: :destroy

  def Picture.placeholder_url
    ActionController::Base.helpers.asset_path("ninelifes-logo.png")
  end

  def is_avatar?
    !self.avatarable_id.nil?
  end

  def is_picture?
    !self.pictureowner_id.nil?
  end

  protected

    def remove_picfile
      pf = picture.current_path
      File.delete("#{pf}")
      FileUtils.remove_dir("#{pf[/^.*\//][0..-2]}")
    end

  private
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
end

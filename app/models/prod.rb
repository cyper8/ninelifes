class Prod < ActiveRecord::Base
  include PgSearch
  has_many :bookmarks, dependent: :destroy, inverse_of: :prod
  has_many :bookmarked_by, through: :bookmarks, source: :user
  has_many :pictures, as: :pictureowner, dependent: :delete_all
  has_many :comments, as: :commentable, dependent: :delete_all
  has_many :interested, through: :comments, source: :author
  has_many :tags, as: :taggable, dependent: :destroy
  has_many :alike, through: :tags, source: :taggable, source_type: "Prod"
  belongs_to :owner, class_name: "User"
  validates :title, presence: true, length: {maximum: 255}
  validates :owner, presence: true
  validates :description, presence: true, length: {maximum: 4095}
  default_scope -> { order(updated_at: :asc) }
  scope :created_after, ->(time) { where("created_at >= ?", time) }

  pg_search_scope :search, against: [:title, :subtitle, :description],
    associated_against: { tags: :name, owner: :name }

  @@comments_per_page = 3

  def Prod.comments_per_page
    @@comments_per_page
  end

  def bookmarked?(user)
      self.bookmarked_by.include?(user)
  end

  def pictures_attributes=(parameters)
    parameters.each do |pic,action|
      if action["remove_picture"] == "1"
        self.pictures.find(action["id"]).destroy
      end
    end
  end

  def add_pictures=(parameters)
    if !parameters["pictures"].nil?
      parameters["pictures"].each do |pic|
        self.pictures.build(picture: pic)
      end
    end
    if !parameters["remote_picture_url"].empty?
      pic=self.pictures.new
      pic.remote_picture_url=parameters["remote_picture_url"]
      pic.save
    end
  end

  def tojson
    self.as_json(root: true, include: {
      pictures: { root: true },
      bookmarks: { root: true },
      comments: { root: true },
      tags: { root: true }
    }).to_json
  end

  def new_comments_for(time)
    self.comments.created_after(time)
  end
end

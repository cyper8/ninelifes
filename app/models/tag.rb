class Tag < ActiveRecord::Base
  include PgSearch
  belongs_to :taggable, polymorphic: true

  has_many :similar, class_name: "Tag", foreign_key: "name", primary_key: "name"

  validates :name,  presence: true,
                    length: { minimum: 1, maximum: 64 },
                    format: { with: /\A[\p{L}\p{N}_]*\z/i }
  validates :taggable_id, presence: true

  scope :search, ->(by_name) { where("name ~* ?", ".*#{by_name}.*") }

  scope :list, -> {
    select(:name).distinct
  }

  # scope :include_prod_count, -> {
  #   joins("INNER JOIN (SELECT name, COUNT(name) AS cnt FROM tags GROUP BY name) AS tc ON tc.name = tags.name")
  #   .select("tc.cnt AS prod_count")
  # }
end

class User < ActiveRecord::Base

  has_many :bookmarks, dependent: :destroy, inverse_of: :user
  has_many :favourites, through: :bookmarks, source: :prod
  has_many :comments, dependent: :destroy, foreign_key: "author_id", inverse_of: :author
  has_many :interests, through: :comments, source: :commentable, source_type: "Prod"
  has_many :prod_comments, -> {where commentable_type: "Prod"},
                    class_name: "Comment",
                    foreign_key: "author_id"
  has_many :replies, -> {where commentable_type: "Comment"},
                    class_name: "Comment",
                    foreign_key: "author_id"
  has_many :messages, -> { where(commentable_type: "MessageGroup").unscope(:order).order(created_at: :desc) },
                    class_name: "Comment",
                    foreign_key: "author_id"
  has_many :conversations, class_name: "MgParticipant", dependent: :destroy
  has_many :chats, through: :conversations, source: :message_group
  has_many :message_groups, dependent: :destroy, foreign_key: "moderator_id", inverse_of: :moderator
  has_many :prods, dependent: :destroy, foreign_key: "owner_id", inverse_of: :owner
  has_one :avatarfile, class_name: "Picture", as: :avatarable, dependent: :destroy
  has_one :subscription, dependent: :destroy

  default_scope -> { where(activated: true).order(last_visit: :asc, updated_at: :asc) }

  attr_accessor :remember_token, :activation_token, :reset_token, :remove_avatar
  before_save   :downcase_email

  has_secure_password

  validates :name, length: {minimum: 6, maximum: 50}
  validates :email, presence: true, if: ':provider == "email"'
  validates :email, length: { maximum: 255 },
    format: { with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i }
  validates :provider, presence: true, format: { with: /\A(email|google|facebook)\z/ }
  validates :uid, presence: true
  validates :password, presence: true, length: { minimum: 8,
    too_short: "should have at least 8 characters" },
    format: { with: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z])/,
    message: "should have either a digit, Uppercase and lowercase letter" },
    allow_nil: true

  scope :unactive, -> { unscope(where: :activated).where(activated: false) }

  scope :search, ->(by_name) { where("name ~* ?", ".*#{by_name}.*") }

  @@actions_per_page = 30

  def User.actions_per_page
    @@actions_per_page
  end

  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def forget
    update_attribute(:remember_digest, nil)
  end

  # def baptize
  #   update_attribute(:admin, true)
  # end

  def authenticated?(token,attribute="remember")
    digest = self.send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  def activate
    activation = {activated: true, activated_at: Time.zone.now}
    if self == User.first && !self.admin?
      activation[:admin] = :true
    end
    update_columns(activation)
  end

  def disable
    update_columns(activated: false, activated_at: nil)
  end

  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest, (self.reset_digest = User.digest(reset_token)))
  end

  def create_activation_digest
    self.activation_token  = User.new_token
    update_attribute(:activation_digest, (self.activation_digest = User.digest(activation_token)))
  end

  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end

  def bookmark(prod)
    bookmarks.create(prod_id: prod.id)
  end

  def unbookmark(prod)
    bookmarks.find_by(prod_id: prod.id).destroy
  end

  def self.from_omniauth(usr_hash)
    if !(user = User.find_by(provider: usr_hash[:provider], uid: usr_hash[:uid]))
      user = User.create(usr_hash)
      user.password = User.new_token
      user.activated = true
      user.activated_at = Time.zone.now
      user.save!
    end
    user
  end

  def remove_avatar
    update_attribute(:avatar, nil)
    self.avatarfile.destroy if !self.avatarfile(true).nil?
  end

  def feed(scope_names: ["comments","bookmarks"], where_opts: nil, orderby: "updated_at", asc: true, limit: @@actions_per_page, offset: 0)
    sub_query = scope_names.map do |s|
      selection = "id as action_id, \'#{s}\' as action_type, updated_at"
      case s
      when "bookmarks"
        selection += ', prod_id as "on", \'Prod\' as target'
      when "comments"
        selection += ', commentable_id as "on", commentable_type as target'
      else
        return
      end
      self.send("#{s}").select(selection).where(where_opts).unscope(:order).to_sql
    end.join(" UNION ")
    User.connection.select_all("\
      SELECT * \
      FROM (#{sub_query}) as a \
      ORDER BY #{orderby} \
      #{"DESC" unless asc} \
      #{"LIMIT #{limit}" unless limit.nil?} \
      OFFSET #{offset}")
  end

  def following
    User
    .joins(prods: :bookmarks)
    .where("bookmarks.user_id = ? AND users.id != ?", self.id, self.id)
  end

  def discussions
    User
    .joins(prods: :comments)
    .where("comments.author_id = ? AND comments.commentable_type = 'Prod' AND users.id != ?", self.id, self.id)
  end

  def unread_messages_count
    self.conversations.includes({message_group: [:messages]}).map {|c| c.unread.count}.reduce(:+)||0
  end

  def contacts
    if self.admin?
      User.unscope(where: :activated).where.not(id: self.id)
    else
      User
      .joins(prods: [:bookmarks, :comments])
      .where("bookmarks.user_id = ? OR comments.author_id = ?", self.id, self.id)
      .where("users.id != ?", self.id)
      .distinct
    end
  end

  def last_interacted_with(user)
    last_comment = self.comments.select(:updated_at).where(commentable: user.prods.unscope(:order)).unscope(:order).to_sql
    last_like = self.bookmarks.select(:updated_at).where(prod: user.prods.unscope(:order)).unscope(:order).to_sql
    dates = User.connection.select_all("SELECT a.updated_at FROM (#{last_comment} UNION #{last_like}) as a ORDER BY updated_at DESC")
    if dates.any?
      dates[0]["updated_at"]
    else
      DateTime.new(0)
    end
  end

  def notify(grp,name,msg)
    unless self.subscription.nil?
      message = JSON.generate({
        title: "NineLifes/#{name||""}@#{msg.author.name}:",
        body: "#{msg.content}",
        tag:"#{grp}"})
      subscription = self.subscription
      begin
        Webpush.payload_send(
          message: message,
          endpoint: subscription.endpoint,
          p256dh: subscription.key,
          auth: subscription.auth,
          vapid: {
            subject: "mailto:ninelifes@ya64.uk",
            public_key: Rails.application.config.vapidKey.public_key,
            private_key: Rails.application.config.vapidKey.private_key
          }
        )
      rescue
        return
      end
    end
  end

  private
    # Converts email to all lower-case.
    def downcase_email
      self.email = email.downcase
    end

end

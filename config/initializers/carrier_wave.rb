if Rails.env.production?
  CarrierWave.configure do |config|
    config.fog_provider = 'fog/google'
    config.fog_credentials = {
      # # Configuration for Amazon S3
      # :provider              => 'AWS',
      # :aws_access_key_id     => ENV['AWS_S3_ACCESS_KEY'],
      # :aws_secret_access_key => ENV['AWS_S3_SECRET_KEY'],
      # :region                 => ENV['AWS_S3_REGION']
      provider: 'Google',
      google_project: 'ninelifes-1279',
      google_client_email: 'ninelifes-file-bucket-acces-38@ninelifes-1279.iam.gserviceaccount.com',
      #google_json_key_location: File.expand_path('~/workspace/config/certs/NineLifes-be9ca7637da1.json')
      google_json_key_string: ENV["GOOGLE_CLOUD_STORAGE"]
    }
    config.fog_directory     =  'staging.ninelifes-1279.appspot.com'
    
    config.permissions = 0640
    config.directory_permissions = 0750
  end
else
  CarrierWave.configure do |config|
    config.ignore_integrity_errors = false
    config.ignore_processing_errors = false
    config.ignore_download_errors = false
    config.permissions = 0666
    config.directory_permissions = 0777
  end
end

module CoreExtensions
  class ::Integer
    def put_within(*args)
      if args.length
        if args.length>1
          mn,mx = args[0],args[1]
        else
          mn,mx = 0,args[0]
        end
        if mn==mx
          mn,mx = 0,mx
        end
        if mn>mx
          mn,mx = mx,mn
        end
        case
        when self<mn
          mn
        when self>mx
          mx
        else
          self
        end
      end
    end
  end
end

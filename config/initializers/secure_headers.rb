 SecureHeaders::Configuration.default do |config|
  config.cookies = {
    secure: true, # mark all cookies as "Secure"
    httponly: true, # mark all cookies as "HttpOnly"
    samesite: {
      lax: true # mark all cookies as SameSite=lax
    }
  }
  # Add "; preload" and submit the site to hstspreload.org for best protection.
  #config.hsts = "max-age=#{1.week.to_i}"
  config.x_frame_options = "DENY"
  config.x_content_type_options = "nosniff"
  config.x_xss_protection = "1; mode=block"
  config.x_download_options = "noopen"
  config.x_permitted_cross_domain_policies = "none"
  config.referrer_policy = %w(same-origin) #%w(origin-when-cross-origin strict-origin-when-cross-origin)
  config.csp = {
    # "meta" values. these will shape the header, but the values are not included in the header.
    preserve_schemes: false, # default: false. Schemes are removed from host sources to save bytes and discourage mixed content.

    # directive values: these values will directly translate into source directives
    default_src: %w('none'),
    base_uri: %w('self'),
    #block_all_mixed_content: true, # see http://www.w3.org/TR/mixed-content/
    #child_src: %w('self'), # if child-src isn't supported, the value for frame-src will be set.
    connect_src: %w('self' *.googleapis.com https://fonts.gstatic.com https://secure.gravatar.com https://accounts.google.com *.facebook.com *.googleusercontent.com *.bootstrapcdn.com https://cdn.jsdelivr.net),
    font_src: %w('self' https://fonts.gstatic.com *.bootstrapcdn.com),
    form_action: %w('self'),
    #frame_ancestors: %w('none'),
    img_src: %w('self' blob: data: *),
    manifest_src: %w('self'),
    media_src: %w('self'),
    #object_src: %w('self'),
    sandbox: %w(allow-scripts allow-same-origin allow-modals allow-forms), # true and [] will set a maximally restrictive setting
    #plugin_types: %w(),
    script_src: %w('self' 'unsafe-inline' 'unsafe-eval'),
    style_src: %w('self' 'unsafe-inline' https://fonts.googleapis.com *.bootstrapcdn.com),
    worker_src: %w('self'),
    #upgrade_insecure_requests: true, # see https://www.w3.org/TR/upgrade-insecure-requests/
    report_uri: %w(https://ninelifes.report-uri.com/r/d/csp/reportOnly)
  }
  # This is available only from 3.5.0; use the `report_only: true` setting for 3.4.1 and below.
  # config.csp_report_only = config.csp.merge({
  #   img_src: %w(*),
  #   report_uri: %w(https://ninelifes.report-uri.com/r/d/csp/reportOnly)
  # })
 end

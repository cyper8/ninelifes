Rails.application.routes.draw do

  root             'application#home'
  post "/" => "application#home", param: :filter, as: 'home_filtering'

  get 'signup' => 'users#new'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'
  get '/auth/:provider/callback', to: 'sessions#create'
  get 'search' => 'application#search'
  post 'search' => 'application#search', param: :filter
  # post 'prods' => 'prods#index', param: :filter, as: 'search_prods'
  delete 'pictures/:id', to: 'pictures#destroy', as: 'destroy_picture'

  resources :users do
    member do
      get :text
      post :subscribe , param: :subscription
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :tags,                only: [:index] do
    delete action: :destroy, on: :member, as: 'un'
    collection do
      post 'tag/:prod_id', action: :create, param: :name, as: 'create'
      get ':name', action: :show, as: 'one_of'
    end
  end

  resources :bookmarks,           only: [:create,:destroy]
  resources :comments, except: [:index, :new] do
    member do
      get 'comment', as: 'reply_on'
      get 'replies', param: :page, as: 'replies_on'
    end
  end

  resources :prods do
    member do
      get 'comments', as: 'comments_on'
    end
  end

  resources :message_groups do
    member do
      post 'talk', as: 'talk_to'
      get 'leave', as: 'leave_the'
      get 'messages', param: :page, as: 'messages_to'
    end
  end
  #resources :relationships,       only: [:create, :destroy]
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!( uid: "su@example.com",
              name: "Sample User",
              provider: "email",
              email: "su@example.com",
              password: "passW0rd",
              password_confirmation: "passW0rd",
              activated: true,
              activated_at: Time.zone.now,
              admin: true)
              
29.times do |n|
  provider = "email"
  name = Faker::Name.name
  uid = Faker::Internet.email#"su#{n+1}@example.com"
  email = uid
  password = "passW0rd"
  User.create!( uid: uid, provider: provider, name: name, email: email,
                password: password,
                password_confirmation: password,
                activated: true,
                activated_at: Time.zone.now,
                admin:false)
end

users = User.order(:created_at).take(6)

30.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end

30.times do
  title = Faker::Commerce.product_name
  content = Faker::Lorem.sentence(7)
  prod = Prod.create!(title: title, subtitle: title.reverse, description: content)
  5.times do
    prod.pictures.create!(picture: Faker::Avatar.image)
  end
end
# Following relationships

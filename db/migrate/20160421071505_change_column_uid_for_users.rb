class ChangeColumnUidForUsers < ActiveRecord::Migration
  def change
    change_column :users, :uid, :integer, null:false
  end
end

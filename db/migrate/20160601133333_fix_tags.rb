class FixTags < ActiveRecord::Migration
  def change
    change_column :tags, :name, :string, null: false
    remove_index :tags, column: :name
    add_index :tags, [:name, :taggable_id, :taggable_type], unique: true
  end
end

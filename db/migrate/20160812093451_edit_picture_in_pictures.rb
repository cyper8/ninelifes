class EditPictureInPictures < ActiveRecord::Migration
  def change
    change_column :pictures, :picture, :string
  end
end

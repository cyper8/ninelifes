class RemoveConstraintInPictures < ActiveRecord::Migration
  def change
    change_column_null(:pictures, :picture, true)
  end
end

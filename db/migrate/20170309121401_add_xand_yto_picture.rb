class AddXandYtoPicture < ActiveRecord::Migration
  def change
    add_column :pictures, :x, :integer, default: 0
    add_column :pictures, :y, :integer, default: 0
  end
end

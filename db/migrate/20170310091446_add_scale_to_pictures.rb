class AddScaleToPictures < ActiveRecord::Migration
  def change
    add_column :pictures, :scale, :float, default: 1
  end
end

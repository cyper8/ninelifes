class CreateMessageGroups < ActiveRecord::Migration
  def change
    create_table :message_groups do |t|
      t.string :name
      t.integer :moderator_id, index: true, null: false
      t.timestamps null: false
    end
    add_foreign_key :message_groups, :users, column: :moderator_id, delete: :cascade
  end
end

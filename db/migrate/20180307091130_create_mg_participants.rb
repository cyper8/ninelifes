class CreateMgParticipants < ActiveRecord::Migration
  def change
    create_table :mg_participants do |t|
      t.references :message_group, index: true, null: false, foreign_key: true
      t.references :user, index: true, null: false, foreign_key: true

      t.timestamps null: false
    end
    add_index :mg_participants, [:message_group_id, :user_id], unique: true
  end
end

class AddOwnerToProd < ActiveRecord::Migration
  def change
    add_reference :prods, :owner, index: true, null: false
    add_foreign_key :prods, :users, column: :owner_id
  end
end

class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.references :user, index: true, foreign_key: true
      t.string :endpoint
      t.string :auth
      t.string :key

      t.timestamps null: false
    end
  end
end

class CreateUsers < ActiveRecord::Migration
  def change
    drop_table :users
    create_table :users do |t|
      t.string   :uid, null: false, unique: true
      t.string   :provider, null: false
      t.string   :name,              limit: 50
      t.string   :email,             limit: 255                 
      t.string   :usrdata
      t.string   :password_digest
      t.string   :remember_digest
      t.boolean  :admin,                         default: false
      t.string   :activation_digest
      t.boolean  :activated,                     default: false
      t.datetime :activated_at
      t.string   :reset_digest
      t.datetime :reset_sent_at
      
      t.timestamps null: false
    end
  
    add_index :users, :provider
    add_index :users, :uid
    add_index :users, [:provider, :uid], unique: true

  end
end

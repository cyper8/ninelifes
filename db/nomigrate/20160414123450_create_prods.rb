class CreateProds < ActiveRecord::Migration
  def change
    create_table :prods do |t|
      t.string :title, null: false
      t.string :subtitle
      t.text :description, null: false

      t.timestamps null: false
    end
  end
end

class CreateBookmarks < ActiveRecord::Migration
  def change
    create_table :bookmarks do |t|
      t.references :user, index: true, foreign_key: true
      t.references :prod, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :bookmarks, [:user_id, :prod_id], unique: true
  end
end

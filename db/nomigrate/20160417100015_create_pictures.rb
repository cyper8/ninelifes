class CreatePictures < ActiveRecord::Migration
  def change
    create_table :pictures do |t|
      t.string :picture, null: false
      t.references :avatarable, polymorphic: true, index: true
      t.references :pictureowner, polymorphic: true, index: true
      t.timestamps null: false
    end
  end
end

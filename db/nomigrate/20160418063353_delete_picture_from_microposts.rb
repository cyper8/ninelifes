class DeletePictureFromMicroposts < ActiveRecord::Migration
  def change
    remove_column :microposts, :picture
  end
end

class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.text :content, null: false
      t.references :commentable, polymorphic: true, index: true
      t.references :replieable, polymorphic: true, index: true
      t.references :author, index: true, foreign_key: true
      
      t.timestamps null: false
    end
    add_index :comments, [:author_id, :created_at]
  end
end

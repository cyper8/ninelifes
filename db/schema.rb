# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180426070609) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "unaccent"
  enable_extension "pg_trgm"

  create_table "bookmarks", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "prod_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "bookmarks", ["prod_id"], name: "index_bookmarks_on_prod_id", using: :btree
  add_index "bookmarks", ["user_id", "prod_id"], name: "index_bookmarks_on_user_id_and_prod_id", unique: true, using: :btree
  add_index "bookmarks", ["user_id"], name: "index_bookmarks_on_user_id", using: :btree

  create_table "comments", force: :cascade do |t|
    t.text     "content",          null: false
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "replieable_id"
    t.string   "replieable_type"
    t.integer  "author_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "comments", ["author_id", "created_at"], name: "index_comments_on_author_id_and_created_at", using: :btree
  add_index "comments", ["author_id"], name: "index_comments_on_author_id", using: :btree
  add_index "comments", ["commentable_type", "commentable_id"], name: "index_comments_on_commentable_type_and_commentable_id", using: :btree
  add_index "comments", ["replieable_type", "replieable_id"], name: "index_comments_on_replieable_type_and_replieable_id", using: :btree

  create_table "message_groups", force: :cascade do |t|
    t.string   "name"
    t.integer  "moderator_id", null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  add_index "message_groups", ["moderator_id"], name: "index_message_groups_on_moderator_id", using: :btree

  create_table "mg_participants", force: :cascade do |t|
    t.integer  "message_group_id", null: false
    t.integer  "user_id",          null: false
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "mg_participants", ["message_group_id", "user_id"], name: "index_mg_participants_on_message_group_id_and_user_id", unique: true, using: :btree
  add_index "mg_participants", ["message_group_id"], name: "index_mg_participants_on_message_group_id", using: :btree
  add_index "mg_participants", ["user_id"], name: "index_mg_participants_on_user_id", using: :btree

  create_table "pg_search_documents", force: :cascade do |t|
    t.text     "content"
    t.integer  "searchable_id"
    t.string   "searchable_type"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "pg_search_documents", ["searchable_type", "searchable_id"], name: "index_pg_search_documents_on_searchable_type_and_searchable_id", using: :btree

  create_table "pictures", force: :cascade do |t|
    t.string   "picture"
    t.integer  "avatarable_id"
    t.string   "avatarable_type"
    t.integer  "pictureowner_id"
    t.string   "pictureowner_type"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "x",                 default: 0
    t.integer  "y",                 default: 0
    t.float    "scale",             default: 1.0
  end

  add_index "pictures", ["avatarable_type", "avatarable_id"], name: "index_pictures_on_avatarable_type_and_avatarable_id", using: :btree
  add_index "pictures", ["pictureowner_type", "pictureowner_id"], name: "index_pictures_on_pictureowner_type_and_pictureowner_id", using: :btree

  create_table "prods", force: :cascade do |t|
    t.string   "title",       null: false
    t.string   "subtitle"
    t.text     "description", null: false
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "owner_id",    null: false
  end

  add_index "prods", ["owner_id"], name: "index_prods_on_owner_id", using: :btree

  create_table "subscriptions", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "endpoint"
    t.string   "auth"
    t.string   "key"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "subscriptions", ["user_id"], name: "index_subscriptions_on_user_id", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string   "name",          null: false
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  add_index "tags", ["name", "taggable_id", "taggable_type"], name: "index_tags_on_name_and_taggable_id_and_taggable_type", unique: true, using: :btree
  add_index "tags", ["taggable_type", "taggable_id"], name: "index_tags_on_taggable_type_and_taggable_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "uid",                                           null: false
    t.string   "provider",                                      null: false
    t.string   "name",              limit: 50,                  null: false
    t.string   "email",             limit: 255
    t.string   "usrdata"
    t.string   "password_digest",                               null: false
    t.string   "remember_digest"
    t.boolean  "admin",                         default: false
    t.string   "avatar"
    t.string   "activation_digest"
    t.boolean  "activated",                     default: false
    t.datetime "activated_at"
    t.string   "reset_digest"
    t.datetime "reset_sent_at"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.datetime "last_visit"
  end

  add_index "users", ["provider", "uid"], name: "index_users_on_provider_and_uid", unique: true, using: :btree
  add_index "users", ["provider"], name: "index_users_on_provider", using: :btree
  add_index "users", ["uid"], name: "index_users_on_uid", using: :btree

  add_foreign_key "message_groups", "users", column: "moderator_id"
  add_foreign_key "mg_participants", "message_groups"
  add_foreign_key "mg_participants", "users"
  add_foreign_key "prods", "users", column: "owner_id"
  add_foreign_key "subscriptions", "users"
end

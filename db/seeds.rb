# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.create!( uid: "su@example.com",
              name: "Sample User",
              provider: "email",
              email: "su@example.com",
              password: "passW0rd",
              password_confirmation: "passW0rd",
              activated: true,
              activated_at: Time.zone.now,
              admin: true)

30.times do |n|
  provider = "email"
  name = Faker::Name.name
  uid = Faker::Internet.email#"su#{n+1}@example.com"
  email = uid
  password = "passW0rd"
  user = User.create!( uid: uid, provider: provider, name: name, email: email,
                password: password,
                password_confirmation: password,
                activated: true,
                activated_at: Time.zone.now,
                admin:false)
end


30.times do |n|
  title = Faker::Commerce.product_name
  content = Faker::Lorem.sentence(170)
  prod = User.find(n+1).prods.create(title: title, subtitle: title.reverse, description: content)
  ptag = Faker::Commerce.color
  5.times do |i|
   c_content = Faker::Lorem.sentence(5)
   c_author = User.find(i+2)
   prod.bookmarks.create!(user: c_author)
   comment = prod.comments.create!(content: c_content, author_id: c_author.id)
   comment.replies.create!(content: "Thanks for review!", author_id: User.first.id)
  end
  4.times do |p|
    picfile = File.open("test/fixtures/pics/l"+(n+(30*p)).to_s+".jpg")
    # picfile = "https://loremflickr.com/400/400"
    ppic = prod.pictures.new
    ppic.picture = picfile
    # ppic.picture.download!(picfile)
    ppic.save!
  end
  prod.tags.create!(name: (ptag.sub(/ /,"_") || ptag))
end

Prod.first.tags.create!(name: "first")
9.times { |i| Prod.find(i+2).tags.create!(name: "old") }
(10..19).each { |i| Prod.find(i).tags.create!(name: "best") }

5.times do |mg|
  user = User.find(mg+1)
  mgrp = user.message_groups.create!(name: "mg#{mg}")
  4.times do |mu|
    mgrp.participants.reload.create!(user: user.contacts[mu])
  end
  4.times do |m|
    mgrp.messages.create!(author: mgrp.reload.conversants[m],content: "Message#{m}")
  end
end

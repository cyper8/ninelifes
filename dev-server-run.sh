#!/bin/bash

# stop if no .env
[ ! -f .env ] && exit 1

# setup environment if not already done
for v in `cat .env`; do
  _v=${v%%=*}
  if [ "${!_v}x" == "x" ]; then
    export $v
  fi
done

# install waiter
if [ ! -f ./wait-for-it/wait-for-it.sh ]; then
  git clone https://github.com/vishnubob/wait-for-it.git
fi

# cleanup some old process pids (needed in docker since he doesn't bother to remove it when stopping the container)
rm -f tmp/pids/*

# setup db if not already
rake db:exists && rake db:migrate || rake db:setup

# prepare db hostname and port for wait-for-it

_db_host=${DATABASE_URL:-$DB_HOST}
_db_host=${_db_host:-localhost}
_db_host=${_db_host##*@}
_db_host=${_db_host%%/*}
_db_port=${_db_host##*:}
_db_port=${_db_port:-$DB_PORT}
_db_port=${_db_port:-5432}
_db_host=${_db_host%%:*}

# DB_PORT=$_db_port
# DB_HOST=$_db_host

# kick server up as long as db is up
./wait-for-it/wait-for-it.sh ${_db_host}:${_db_port} && bundle exec rails s -p ${NINELIFES_PORT:-${PORT:-3000}} -b "${NINELIFES_IP:-${IP:-0.0.0.0}}"


class SelectiveUglifier
  def self.call(input)
    if input[:filename] =~ /\.min\.js/
      input[:data]
    else
      @options = {harmony: true}
      
      case Uglifier::VERSION.to_i
      when 1
        raise "uglifier 1.x is no longer supported, please upgrade to 2.x or newer"
      when 2
        input_options = { source_filename: input[:filename] }
      else
        input_options = { source_map: { filename: input[:filename] } }
      end

      uglifier = Uglifier.new(@options.merge(input_options))

      js, map = uglifier.compile_with_map(input[:data])

      { data: js, map: map }
    end
  end
end
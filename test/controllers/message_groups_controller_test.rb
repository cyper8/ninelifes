require 'test_helper'

class MessageGroupsControllerTest < ActionController::TestCase
  test "create new Message Group with 3 users" do
    lig_in_as users(:admin)
    post :create, name: "Test Group", users: [1,3,5]
    assert_response :success
  end
end

require 'test_helper'

class PasswordResetsTest < ActionDispatch::IntegrationTest
  
  def setup
    ActionMailer::Base.deliveries.clear
    @user = users(:test)
  end
  
  test "password resets" do
    get new_password_reset_path
    assert_template 'password_resets/new', "Wrong entry"
    post password_resets_path, password_reset: { email: "" }
    assert_not flash.empty?, "No error message on empty email"
    assert_template 'password_resets/new', "wrong redirect on empty email"
    post password_resets_path, password_reset: { email: @user.email }
    assert_not_equal @user.reset_digest, @user.reload.reset_digest, "No new token digest"
    assert_equal 1, ActionMailer::Base.deliveries.size, "no mail sent"
    assert_not flash.empty?
    assert_redirected_to root_url
    user = assigns(:user)
    get edit_password_reset_path(user.reset_token, email: "")
    assert_redirected_to root_url, "no red to home on invalid reset link (no email)"
    user.toggle!(:activated)
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_redirected_to root_url, "no red to home on valid reset for inactive user"
    user.toggle!(:activated)
    get edit_password_reset_path('wrong_token', email: user.email)
    assert_redirected_to root_url, "no red to home on invalid reset link (wrong token)"
    get edit_password_reset_path(user.reset_token, email: user.email)
    assert_template 'password_resets/edit', "no reset form on valid reset link"
    assert_select "input[name=email][type=hidden][value=?]", user.email
    patch password_reset_path(user.reset_token),
          email: user.email,
          user: { password:              "foobaz",
                  password_confirmation: "barquux" }
    assert_select 'div#error_explanation'
    # Empty password
    patch password_reset_path(user.reset_token),
          email: user.email,
          user: { password:              "",
                  password_confirmation: "" }
    assert_select 'div#error_explanation'
    # Valid password & confirmation
    @user.update_attribute(:reset_sent_at, 3.hours.ago)
    patch password_reset_path(user.reset_token),
          email: user.email,
          user: { password:              "F0obaz",
                  password_confirmation: "F0obaz" }
    assert_response :redirect
    follow_redirect!
    assert_match /expired/i, response.body
    @user.update_attribute(:reset_sent_at, 1.hours.ago)
    patch password_reset_path(user.reset_token),
          email: user.email,
          user: { password:              "F0obaz",
                  password_confirmation: "F0obaz" }
    assert_not flash.empty?
    assert_redirected_to login_url
  end
end

require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:test)
    @other_user = users(:admin)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit', "Not an edit page before unsuccessful edit"
    patch user_path(@user), user: { name:  "",
                                    email: "foo@invalid",
                                    password:              "foo",
                                    password_confirmation: "bar" }
    assert_template 'users/edit', "Not an edit page after unsuccessful edit!"
    assert_not flash.empty?, "Flash message not set: " + flash.to_hash.inspect
  end
  
  test "successful edit with friendly forwarding" do
    get edit_user_path(@user) #forgot to log in - must be redirected to a login page
    assert_redirected_to login_path, "Not redirected to a login upon unauthorized request for edit"
    log_in_as(@user) #Got logged in
    assert_redirected_to edit_user_path(@user), "Not redirected to edit page upon login"
    name  = "Foo Bar"
    email = "foo@bar.com"
    patch user_path(@user), user: { name:  name,
                                    email: email,
                                    password:              "",
                                    password_confirmation: "" }
    assert_not flash[:success].nil?, "Unexpectedly Flash says: "+flash.to_s
    assert_redirected_to @user, "Not redirected to a profile page after successful edit"
    @user.reload
    assert_equal name,  @user.name, "Name is not edited"
    assert_equal email, @user.email, "Email is not edited"
  end
  
  test "successful edit with priviledges elevation attempt" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit', "Not an edit page before unsuccessful edit"
    patch user_path(@user), user: { name:  @user.name,
                                    email: @user.email,
                                    password:              "",
                                    password_confirmation: "",
                                    admin: true }
    assert_not flash[:success].nil?, "Unexpectedly Flash says: "+flash.to_s
    assert_redirected_to @user, "Not redirected to a profile page after successful edit"
    @user.reload
    assert_not is_admin?, "Hacked!"
  end
    
end

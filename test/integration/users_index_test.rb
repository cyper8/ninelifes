require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest
  def setup
    @admin = users(:admin)
    @non_admin = users("user_#{rand(30)}")
  end

  test "index as admin including pagination and delete links" do
    log_in_as(@admin)
    @non_admin.update_attributes(:activated => false)
    get users_path
    assert_template 'users/index', "No index for admin"
    User.where(activated: true).each do |user|
      assert_select 'h1', text: user.name
      assert_select 'a[href=?]', user_path(user), text: "Profile"
      assert_select 'a', text: @non_admin.name, count: 0
    end
    assert_difference 'User.count', -1 do
      delete user_path(@non_admin)
    end
  end

  test "index as non-admin" do
    log_in_as(@non_admin)
    get users_path
    assert_select 'a', text: 'delete', count: 0
  end
end

require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper
  
  def setup
    @user = users(:test)
  end
  
  test "profile display" do
    log_in_as @user #what the heck
    get user_path(@user)
    assert_template 'users/show'
    assert_select 'title', full_title(@user.name)
    assert_select 'h1', text: @user.name
    assert_select 'img.gravatar'
    assert_match @user.microposts.count.to_s, response.body
    #assert_select 'div.pagination'
    @user.microposts.each do |micropost|
      assert_select 'span.content', text: micropost.content
    end
  end
end

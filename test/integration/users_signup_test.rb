require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  
  def setup
    ActionMailer::Base.deliveries.clear
  end
  
  test "invalid signup information" do
    get signup_path
    assert_no_difference 'User.count' do
      post users_path, user: { name:  "a",
                               email: "b@c",
                               password:              "d",
                               password_confirmation: "e" }
    end
    assert_template 'users/new'
    assert_select 'div#error_explanation' do
      assert_select 'ul li', 5
    end
    assert_select 'div.field_with_errors', 8
  end
  
  test "valid signup information with activation and adminization attempt" do
    get signup_path
    assert_difference 'User.count', 1 do
      post users_path, user: { name:  "Example User",
                                            email: "user@example.com",
                                            password:              "passW0rd",
                                            password_confirmation: "passW0rd",
                                            admin: true}
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user)
    assert_not user.activated?
    log_in_as(user)
    assert_not is_logged_in?
    get edit_account_activation_path('invalid token')
    assert_not user.activated?, "activated by invalid token"
    get edit_account_activation_path(user.activation_token, email: 'wrong')
    assert_not user.activated?, "activated by invalid email"
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?, "Valid activation failed somehow"
    log_in_as(user)
    follow_redirect!
    assert_template 'users/show', "Not at the profile"
    assert is_logged_in?, "Not logged in after all"
    assert_not is_admin?, "Hacked!"
  end
    
end

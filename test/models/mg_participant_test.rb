require 'test_helper'

class MgParticipantTest < ActiveSupport::TestCase
  test "should have total 4 participants" do
    assert_equal 4, MgParticipant.count
  end
end

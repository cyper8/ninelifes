ENV['RAILS_ENV'] ||= 'test'
require 'fileutils'
require File.expand_path('../../config/environment', __FILE__)
require 'rails/test_help'
require "minitest/reporters"
Minitest::Reporters.use!

class ActiveSupport::TestCase
  fixtures :all

  # Carrierwave setup and teardown
  carrierwave_template = Rails.root.join('test','fixtures','pics')
  carrierwave_root = Rails.root.join('test','uploads')

  # Carrierwave configuration is set here instead of in initializer
  CarrierWave.configure do |config|
    config.root = carrierwave_root
    config.storage = :file
    config.enable_processing = false
    config.ignore_integrity_errors = false
    config.ignore_processing_errors = false
    config.ignore_download_errors = false
    config.permissions = 0666
    config.directory_permissions = 0777
    config.cache_dir = Rails.root.join('test','uploads','carrierwave_cache')
  end

  # And copy carrierwave template in
  #puts "Copying\n  #{carrierwave_template.join('uploads').to_s} to\n  #{carrierwave_root.to_s}"
  FileUtils.cp_r carrierwave_template, carrierwave_root

  # Returns true if a test user is logged in.
  def is_logged_in?
    !session[:user_id].nil?
  end

  # Logs in a test user.
  def log_in_as(user, options = {})
    password    = user.password || options[:password]    || 'passW0rd'
    remember_me = options[:remember_me] || '1'
    if integration_test?
      post login_path, session: { email:       user.email,
                                  password:    password,
                                  remember_me: remember_me }
    else
      session[:user_id] = user.id
    end
  end

  def is_admin?
    User.find(session[:user_id]).admin
  end

  at_exit do
    #puts "Removing carrierwave test directories:"
    Dir.glob(carrierwave_root.join('*')).each do |dir|
      #puts "   #{dir}"
      FileUtils.remove_entry(dir)
    end
  end

  private

    # Returns true inside an integration test.
    def integration_test?
      defined?(post_via_redirect)
    end
end
